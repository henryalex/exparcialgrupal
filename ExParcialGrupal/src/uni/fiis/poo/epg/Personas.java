package uni.fiis.poo.epg;

public class Personas {
    protected String dni;
    protected String nombre;
    protected String apellido;
    protected String correo;
    protected String telefono;
    protected String universidad;

    public Personas(String dni, String nombre, String apellido, String correo, String telefono, String universidad) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.telefono = telefono;
        this.universidad = universidad;
    }



    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }


}
