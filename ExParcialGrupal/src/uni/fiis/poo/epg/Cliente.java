package uni.fiis.poo.epg;

public class Cliente extends Personas {

    private String direccion;
    private Curso curso;
    private Profesor profesor;

    public Cliente(String dni, String nombre, String apellido, String correo, String telefono, String universidad, String direccion) {
        super(dni, nombre, apellido, correo, telefono, universidad);
        this.direccion = direccion;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    @Override
    public String toString() {
        return "Curso :" + curso.getNombre() +
                "\nProfesor: " + profesor.getNombre() + " " + profesor.getApellido() +
                "\nHorario: " + profesor.getHorarios();
    }
}
