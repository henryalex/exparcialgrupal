package uni.fiis.poo.epg;

public class Administrador {
    private String usuario;
    private String clave;

    public Administrador(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;

    }

    public String getUsuario() {
        return this.usuario;
    }

    public String getClave() {
        return this.clave;
    }


}
