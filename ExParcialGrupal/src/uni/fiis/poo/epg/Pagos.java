package uni.fiis.poo.epg;

public interface Pagos {
    public double obtenerMontoTotal(int costo);
    public void descuento();

}
