package uni.fiis.poo.epg;

import java.util.ArrayList;
import java.util.Scanner;

public class GestionarCurso {
    Scanner sd=new Scanner(System.in);
    ArrayList<Curso> cursoscb = new ArrayList<>();
    ArrayList<Curso> cursosis = new ArrayList<>();


    public void setCursoscb(ArrayList<Curso> cursoscb) {
        this.cursoscb = cursoscb;
    }

    public void setCursosis(ArrayList<Curso> cursosis) {
        this.cursosis = cursosis;
    }

    public void agregarCurso(){
        System.out.println("1.Agregar cursos de ciencias basicas");
        System.out.println("2.Agregar cursos de ingenieria de sistemas");
        System.out.print("Indique la opcion que desea agregar: ");
        int opm = sd.nextInt();
        sd.nextLine();
        //Ingreso de datos del curso
        String cod, nombre, descrip, area;
        int ciclo, precio, horsem;
        System.out.print("Ingrese codigo del curso: ");
        cod = sd.nextLine();
        System.out.print("Ingrese nombre del curso: ");
        nombre = sd.nextLine();
        System.out.print("Ingrese descripcion del curso: ");
        descrip = sd.nextLine();
        System.out.print("Ingrese area del curso: ");
        area = sd.nextLine();
        System.out.print("Ingrese ciclo del curso: ");
        ciclo = sd.nextInt();
        System.out.print("Ingrese precio del curso: ");
        precio = sd.nextInt();
        System.out.print("Ingrese horas semanales del curso: ");
        horsem = sd.nextInt();
        System.out.println("**Temas del curso**");
        ArrayList<String> temasnw1 = new ArrayList<>();
        System.out.print("Cuantos temas contiene el curso?: ");
        int ntemas = sd.nextInt();
        sd.nextLine();
        for (int i = 0; i < ntemas; i++) {
            System.out.print("Ingrese tema: ");
            String tem = sd.nextLine();
            temasnw1.add(tem);
        }
        System.out.println("**Profesores del curso**");
        ArrayList<Profesor> profnw = new ArrayList<>();
        Profesor profenw1 = null;
        System.out.print("Cuantos profesores tendra el curso?: ");
        int nprof = sd.nextInt();
        sd.nextLine();
        for (int i = 0; i < nprof; i++) {
            System.out.println("Ingrese datos del profesor:");
            String dni, nomb, apell, correo, tel, universidad;
            System.out.print("Ingrese el DNI: ");
            dni = sd.nextLine();
            System.out.print("Ingrese el Nombre: ");
            nomb = sd.nextLine();
            System.out.print("Ingrese el Apellido: ");
            apell = sd.nextLine();
            System.out.print("Ingrese el Correo: ");
            correo = sd.nextLine();
            System.out.print("Ingrese el Telefono: ");
            tel = sd.nextLine();
            System.out.print("Indique la universidad a la que pertenece: ");
            universidad = sd.nextLine();
            ArrayList<String> horpw1 = new ArrayList<>();
            System.out.print("Cuantos dias a la semana enseñara el profesor?: ");
            int ds = sd.nextInt();
            sd.nextLine();
            for (int s = 0; s < ds; s++) {
                System.out.print("Ingrese los datos (dia y hora) del " + (s + 1) + " dia: ");
                String hw1 = sd.nextLine();
                horpw1.add(hw1);
            }
            profenw1 = new Profesor(dni, nomb, apell, correo, tel, universidad, horpw1);
            profnw.add(profenw1);
        }
        Curso cursoa = new Curso(cod, nombre, descrip, area, ciclo, precio, horsem, temasnw1, profnw);
        //Agregar Curso ciencias basicas:
        if (opm == 1) {
            cursoscb.add(cursoa);
            System.out.println("Sus datos fueron guradados con exito");
            System.out.println("Lista de cursos:");
            for (Curso cu : cursoscb) {
                System.out.println((cursoscb.indexOf(cu) + 1) + ".- " + cu.getNombre());
            }
            //Agregar Curso ingenieria de sistemas:
        } else if (opm == 2) {
            cursosis.add(cursoa);
            System.out.println("Sus datos fueron guradados con exito");
            System.out.println("Lista de cursos:");
            for (Curso cu : cursosis) {
                System.out.println((cursosis.indexOf(cu) + 1) + ".- " + cu.getNombre());
            }
        }

    }
    public void modificarCurso(){
        boolean aux=false;
        System.out.println("1.Modificar cursos de ciencias basicas");
        System.out.println("2.Modificar cursos de ingenieria de sistemas");
        System.out.print("Indique la opcion que desea modificar:");
        int opm = sd.nextInt();
        sd.nextLine();
        //Modificar curso de ciencias basicas
        if (opm == 1) {
            System.out.println("Introduzca el codigo del curso que desea actualizar: ");
            String op = sd.nextLine();
            Curso cumf = null; //cumf: curso modificado
            for (Curso cu : cursoscb) {
                if (op.equals(cu.getCodigo())) {
                    aux=true;
                    System.out.println("Curso Encontrado: ");
                    System.out.println(cu.toString());
                    cumf = cu;
                    System.out.println("¿Que campo deasea actualizar?:");
                    System.out.println("1.- Codigo");
                    System.out.println("2.- Nombre");
                    System.out.println("3.- Descripcion");
                    System.out.println("4.- Area");
                    System.out.println("5.- Ciclo");
                    System.out.println("6.- Precio");
                    System.out.println("7.- horassemanales");
                    int opc = sd.nextInt();
                    sd.nextLine();
                    System.out.println("Ingrese el nuevo valor: ");
                    switch (opc) {
                        case 1:
                            cumf.setCodigo(sd.nextLine());
                            break;
                        case 2:
                            cumf.setNombre(sd.nextLine());
                            break;
                        case 3:
                            cumf.setDescripcion(sd.nextLine());
                            break;
                        case 4:
                            cumf.setArea(sd.nextLine());
                            if (cumf.getArea().equals("Ingenieria de sistemas")) {
                                cursoscb.remove(cumf);
                                cursosis.add(cumf);
                            }
                            break;
                        case 5:
                            cumf.setCiclo(sd.nextInt());
                            break;
                        case 6:
                            cumf.setPrecio(sd.nextInt());
                            break;
                        case 7:
                            cumf.setHorasemanales(sd.nextInt());
                            break;
                    }
                    System.out.println("Curso Actualizado");


                }

            }
            if(!aux){
                System.out.println("Curso no encontrado");
            }
            //Modificar curso de ingenieria de sistemas
        } else if (opm == 2) {

            System.out.println("\nIntroduzca el codigo del curso que desea actualizar: ");
            String op = sd.nextLine();
            Curso cumf = null; //cumf: curso modificado
            for (Curso cu : cursosis) {
                if (op.equals(cu.getCodigo())) {
                    aux=true;
                    System.out.println("Curso Encontrado: ");
                    System.out.println(cu.toString());
                    cumf = cu;
                    System.out.println("¿Que campo deasea actualizar?:");
                    System.out.println("1.- Codigo");
                    System.out.println("2.- Nombre");
                    System.out.println("3.- Descripcion");
                    System.out.println("4.- Area");
                    System.out.println("5.- Ciclo");
                    System.out.println("6.- Precio");
                    System.out.println("7.- horassemanales");
                    int opc = sd.nextInt();
                    sd.nextLine();
                    System.out.println("Ingrese el nuevo valor: ");
                    switch (opc) {
                        case 1:
                            cumf.setCodigo(sd.nextLine());
                            break;
                        case 2:
                            cumf.setNombre(sd.nextLine());
                            break;
                        case 3:
                            cumf.setDescripcion(sd.nextLine());
                            break;
                        case 4:
                            cumf.setArea(sd.nextLine());
                            if (cumf.getArea().equals("Ciencias basicas")) {
                                cursosis.remove(cumf);
                                cursoscb.add(cumf);
                            }
                            break;
                        case 5:
                            cumf.setCiclo(sd.nextInt());
                            break;
                        case 6:
                            cumf.setPrecio(sd.nextInt());
                            break;
                        case 7:
                            cumf.setHorasemanales(sd.nextInt());
                            break;
                    }
                    System.out.println("Curso Actualizado");
                }
            }
            if(!aux){
                System.out.println("Curso no encontrado");

            }

        }

    }
    public void eliminarCurso(){
        boolean aux=false;
        System.out.println("1.Eliminar cursos de ciencias basicas");
        System.out.println("2.Eliminar cursos de ingenieria de sistemas");
        System.out.print("Indique la opcion que desea eliminar:");
        int opm = sd.nextInt();
        sd.nextLine();
        Object object = null;
        //Eliminar curso de ciencia basica
        if (opm == 1) {
            System.out.println("Indique el codigo del curso que desea eliminar:");
            String codel = sd.nextLine();
            for (Curso mcur : cursoscb) {
                if (codel.equals(mcur.getCodigo())) {
                    object = mcur;
                    aux=true;
                }
            }
            if(aux) {
                cursoscb.remove(object);
                System.out.println("Curso eliminado");
                System.out.println("Lista de cursos:");
                for (Curso cu : cursoscb) {
                    System.out.println((cursoscb.indexOf(cu) + 1) + ".- " + cu.getNombre());
                }
            } else {
                System.out.println("Curso no encontrado");
            }
            //Elimar curso de ingenieria de sistemas
        } else if (opm == 2) {
            System.out.println("Indique el codigo del curso que desea eliminar:");
            String codel = sd.nextLine();
            for (Curso mcur : cursosis) {
                if (codel.equals(mcur.getCodigo())) {
                    object = mcur;
                    aux=true;
                }
            }
            if(aux) {
                cursosis.remove(object);
                System.out.println("Curso eliminado");
                System.out.println("Lista de cursos:");
                for (Curso cu : cursoscb) {
                    System.out.println((cursoscb.indexOf(cu) + 1) + ".- " + cu.getNombre());
                }
            } else {
                System.out.println("Curso no encontrado");
            }
        }

    }


}
