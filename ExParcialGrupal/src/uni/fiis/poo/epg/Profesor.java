
package uni.fiis.poo.epg;

import java.util.ArrayList;

public class Profesor extends Personas {

    private ArrayList<String> horarios;



    public Profesor(String dni, String nombre, String apellido, String correo, String telefono, String universidad, ArrayList<String> horarios) {
        super(dni, nombre, apellido, correo, telefono, universidad);
        this.horarios = horarios;
    }



    public ArrayList<String> getHorarios(){
        return horarios;

    }


    public String toString(){
        String respu = super.getNombre();

        return respu;
    }





}
