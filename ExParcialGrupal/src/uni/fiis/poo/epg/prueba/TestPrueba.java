package uni.fiis.poo.epg.prueba;

import uni.fiis.poo.epg.*;

import java.util.ArrayList;

import java.util.Scanner;

public class TestPrueba {
    public static GestionarCurso gc = new GestionarCurso();

    public static void main(String[] args) {
        Scanner sd = new Scanner(System.in);

        String op;

        //Creacion de administradores:
        Administrador a1 = new Administrador("marcosceotrfg", "34thyu");
        Administrador a2 = new Administrador("antonioadminasop", "78sdfo");
        ArrayList<Administrador> administradores = new ArrayList<>();
        administradores.add(a1);
        administradores.add(a2);

        //Creacion de Horarios disponibles:
        ArrayList<String> horario1 = new ArrayList<>();
        horario1.add("lunes 4:00-6:00");
        horario1.add("miercoles 5:00-7:00");
        horario1.add("viernes 2:00-4:00");
        ArrayList<String> horario2 = new ArrayList<>();
        horario2.add("lunes 5:00-7:00");
        horario2.add("miercoles 6:00-8:00");
        horario2.add("viernes 7:00-9:00");
        ArrayList<String> horario3 = new ArrayList<>();
        horario3.add("lunes 5:00-7:00");
        horario3.add("miercoles 6:00-8:00");
        horario3.add("viernes 7:00-9:00");

        //Creacion de objetos profesor:

        Profesor prof1 = new Profesor("2344434", "Carlos", "Peralta Perez", "cperalta@gmail.com", "987123789", "UNI", horario1);
        Profesor prof2 = new Profesor("2345679", "Martin", "Caceres Lopez", "mcaceres@gmail.com", "934567234", "UNI", horario2);
        Profesor prof3 = new Profesor("2324234", "Jose", "Carranza Cerna", "carranzac@gmail.com", "934643434", "UNI", horario3);
        Profesor prof4 = new Profesor("4354234", "Marcos", "Rodriguez Leguia", "rodriguezl@gmail.com", "95322334", "UNI", horario1);
        //Profesores de ciencias basica:
        ArrayList<Profesor> profesorescb = new ArrayList<>();
        profesorescb.add(prof1);
        profesorescb.add(prof2);
        //Profesores de ingenieria de sistemas:
        ArrayList<Profesor> profesoresis = new ArrayList<>();
        profesoresis.add(prof3);
        profesoresis.add(prof4);

        //Creacion de Cursos para asesoria:
        ArrayList<String> temasc1 = new ArrayList<>();
        temasc1.add("funciones");
        temasc1.add("limites");
        temasc1.add("derivadas");
        Curso curso1 = new Curso("BMA01", "Calculo I", "Matematica de primer ciclo", "Ciencias basicas", 1, 150, 6, temasc1, profesorescb);
        ArrayList<String> temaspoo = new ArrayList<>();
        temaspoo.add("clases");
        temaspoo.add("herencia");
        temaspoo.add("polimorfismo");
        Curso curso2 = new Curso("SI302", "POO", "programacion orientada a objetos", "Ingenieria de sistemas", 3, 250, 4, temaspoo, profesoresis);
        ArrayList<String> temases = new ArrayList<>();
        temases.add("Esdistica descriptiva");
        temases.add("Variable aleatoria");
        temases.add("Distribuciones muestrales");
        Curso curso3 = new Curso("FB305", "Estadistica y Probabilidades", "Estadistica basica", "Ciencias basicas", 3, 150, 5, temases, profesorescb);

        //Lista de cursos disponibles:
        //Cursos de ciencias basicas

        ArrayList<Curso> cursoscb = new ArrayList<>();
        cursoscb.add(curso1);
        cursoscb.add(curso3);
        //Cursos de ingenieria de sistemas
        ArrayList<Curso> cursosis = new ArrayList<>();
        cursosis.add(curso2);

        gc.setCursoscb(cursoscb);
        gc.setCursosis(cursosis);

        System.out.println("---------------------------");
        System.out.println("Bienvenido a HELP UNIVERSITY");
        System.out.println("1.Administrador");
        System.out.println("2.Profesor");
        System.out.println("3.Cliente");
        System.out.println("---------------------------");
        System.out.print("Indique la opcion que le corresponda: ");
        int ophu = sd.nextInt();
        //Administrador:
        if (ophu == 1) {
            sd.nextLine();
            System.out.print("USUARIO: ");
            String us = sd.nextLine();
            System.out.print("CLAVE: ");
            String cl = sd.nextLine();
            boolean aux = false;

            for (Administrador adm : administradores) {
                if (us.equals(adm.getUsuario()) && cl.equals(adm.getClave())) {
                    aux = true;
                }
            }

            if (aux) {
                do {
                    System.out.println("Bienvenido Administrador");
                    System.out.println("-------------------");
                    System.out.println("MENU DE OPCIONES");
                    System.out.println("-------------------");
                    System.out.println("1.Agregar Curso");
                    System.out.println("2.Modificar Curso");
                    System.out.println("3.Eliminar Curso");

                    System.out.println("Escriba el numero de la opcion que desea realizar:");
                    int opci = sd.nextInt();
                    switch (opci) {
                        case 1:
                            agregarCurso();
                            break;
                        case 2:
                            modificarCurso();
                            break;
                        case 3:
                            eliminarCurso();
                            break;

                    }

                    sd.nextLine();
                    System.out.println("Desea realizar otra operacion (s/n):");
                    op = sd.nextLine();
                } while (op.equals("s"));

            } else if (!aux) {
                System.out.println("Acceso denegado: Usuario o clave incorrecta");
            }
            //Registro de Profesor:
        } else if (ophu == 2) {
            System.out.println("1.Profesor de ciencias basicas");
            System.out.println("1.Profesor de ciencias basicas");
            System.out.println("Indique la opcion que le corresponda:");
            int opp = sd.nextInt();
            if (opp == 1) {
                System.out.println("Registre sus datos:");
                String dni, nomb, apell, correo, tel, universidad, direc;
                System.out.print("Ingrese el DNI: ");
                dni = sd.nextLine();
                System.out.print("Ingrese el Nombre: ");
                nomb = sd.nextLine();
                System.out.print("Ingrese el Apellido: ");
                apell = sd.nextLine();
                System.out.print("Ingrese el Correo: ");
                correo = sd.nextLine();
                System.out.print("Ingrese el Telefono: ");
                tel = sd.nextLine();
                System.out.print("Indique la universidad a la que pertenece: ");
                universidad = sd.nextLine();
                ArrayList<String> horpw1 = new ArrayList<>();
                System.out.print("Ingresara su horario: ");
                System.out.print("Cuantos dias a la semana enseñara ?: ");
                int ds = sd.nextInt();
                for (int s = 0; s < ds; s++) {
                    System.out.print("Ingrese el horario del " + s + "dia:");
                    String hw1 = sd.nextLine();
                    horpw1.add(hw1);
                }
                Profesor profenew1 = new Profesor(dni, nomb, apell, correo, tel, universidad, horpw1);
                profesorescb.add(profenew1);
                System.out.println("Sus datos fueron guradados con exito");

            } else if (opp == 2) {
                System.out.println("Registre sus datos:");
                String dni, nomb, apell, correo, tel, universidad, direc;
                System.out.print("Ingrese el DNI: ");
                dni = sd.nextLine();
                System.out.print("Ingrese el Nombre: ");
                nomb = sd.nextLine();
                System.out.print("Ingrese el Apellido: ");
                apell = sd.nextLine();
                System.out.print("Ingrese el Correo: ");
                correo = sd.nextLine();
                System.out.print("Ingrese el Telefono: ");
                tel = sd.nextLine();
                System.out.print("Indique la universidad a la que pertenece: ");
                universidad = sd.nextLine();
                ArrayList<String> horpw1 = new ArrayList<>();
                System.out.print("Ingresara su horario: ");
                System.out.print("Cuantos dias a la semana enseñara ?: ");
                int ds = sd.nextInt();
                for (int s = 0; s < ds; s++) {
                    System.out.print("Ingrese el horario del " + s + "dia:");
                    String hw1 = sd.nextLine();
                    horpw1.add(hw1);
                }
                Profesor profenew1 = new Profesor(dni, nomb, apell, correo, tel, universidad, horpw1);
                profesoresis.add(profenew1);
                System.out.println("Sus datos fueron guardados con exito");
            } else {
                System.out.println("No existe esa opcion");
            }
            //Cliente o usuario:
        } else if (ophu == 3) {
            System.out.println("Cursos que brindamos para aseoria:");
            System.out.println("\nCursos de ciencias basicas:");
            for (Curso cu : cursoscb) {
                System.out.println((cursoscb.indexOf(cu) + 1) + ".- " + cu.getNombre());
            }
            System.out.println("Cursos de carrera de Ingenieria de Sistemas:");
            for (Curso cu : cursosis) {
                System.out.println(cursosis.indexOf(cu) + ".- " + cu.getNombre());
            }
            System.out.print("¿Desea recibir asesoria de algun curso? (s/n): ");
            String re = sd.nextLine();
            if (re.equals("s")) {
                //Registro de cliente:
                System.out.println("Registre sus datos:");
                String dni, nomb, apell, correo, tel, universidad, direc;
                System.out.print("Ingrese el DNI: ");
                dni = sd.nextLine();
                System.out.print("Ingrese el Nombre: ");
                nomb = sd.nextLine();
                System.out.print("Ingrese el Apellido: ");
                apell = sd.nextLine();
                System.out.print("Ingrese el Correo: ");
                correo = sd.nextLine();
                System.out.print("Ingrese el Telefono: ");
                tel = sd.nextLine();
                System.out.print("Indique la universidad a la que pertenece: ");
                universidad = sd.nextLine();
                System.out.print("Indique su direccion: ");
                direc = sd.nextLine();

                Cliente cliente = new Cliente(dni, nomb, apell, correo, tel, universidad, direc);
                System.out.println("Sus datos fueron guradados con exito");

                System.out.println("Indique el area que desea:");
                System.out.println("----------------------------------------------");
                System.out.println("1.CIENCIAS BASICAS");
                System.out.println("2.CURSOS DE CARRERA DE INGENIERIA DE SISTEMAS");
                System.out.println("----------------------------------------------");

                int m = sd.nextInt();
                sd.nextLine();
                //Inscripcion a curso de ciencias basicas
                if (m == 1) {
                    System.out.println("Cursos de ciencias basicas:");
                    for (Curso cu : cursoscb) {
                        System.out.println(cursoscb.indexOf(cu) + 1 + ".- " + cu.getNombre());
                    }
                    System.out.println("Indique el numero del curso en el que desea recibir la asesoria :");
                    int en = sd.nextInt();

                    for (Curso cur1 : cursoscb) {
                        if (en == cursoscb.indexOf(cur1) + 1) {
                            cliente.setCurso(cursoscb.get(en - 1));
                            System.out.println("Estos son los profesores con sus horarios disponibles:");
                            cur1.ObtenerhorariosPorProfe();
                            sd.nextLine();
                            System.out.println("Elija el horario o el profesor que desee:");
                            int rp = sd.nextInt();
                            cliente.setProfesor(cur1.getProfesores().get(rp - 1));

                            System.out.println("Usted se va a inscribir en el siguiente curso:");
                            System.out.println(cliente.toString());

                            String resp = "no";
                            while (resp.equals("no")) {
                                System.out.println("----------------------------------");
                                System.out.println("Disponemos de tres formas de pago:");
                                System.out.println("----------------------------------");
                                System.out.println("1.Pago en cuotas");
                                System.out.println("2.Pago mensual");
                                System.out.println("3.Pago del curso completo");
                                System.out.println("Indique la opcion de pago que efectuara:");
                                int pe = sd.nextInt();
                                AdministradorPagos adp = new AdministradorPagos();
                                Pagos pg1 = adp.obtenerUtilidad(pe);
                                System.out.println("Usted efectuara el monto de:" + pg1.obtenerMontoTotal(cur1.getPrecio()));
                                sd.nextLine();
                                System.out.println("Desea efectuar este metodo de pago?:");
                                resp = sd.nextLine();
                            }
                            System.out.println("Registrado con exito");
                        }
                    }
                    //Inscripcion a curso de ingenieria de sistemas
                } else if (m == 2) {
                    System.out.println("Cursos de ingenieria de sistemas:");
                    for (Curso cu : cursosis) {
                        System.out.println(cursosis.indexOf(cu) + 1 + ".- " + cu.getNombre());
                    }
                    System.out.println("Indique el numero del curso en el que desea recibir la asesoria :");
                    int en = sd.nextInt();
                    for (Curso cur1 : cursosis) {
                        if (en == cursosis.indexOf(cur1) + 1) {
                            cliente.setCurso(cursosis.get(en - 1));
                            System.out.println("Estos son los profesores con sus horarios disponibles:");
                            cur1.ObtenerhorariosPorProfe();
                            sd.nextLine();
                            System.out.println("Elija el horario o el profesor que desee:");
                            int rp = sd.nextInt();
                            cliente.setProfesor(cur1.getProfesores().get(rp - 1));

                            System.out.println("Usted se va a inscribir en el siguiente curso:");
                            System.out.println(cliente.toString());

                            String resp = "no";
                            while (resp.equals("no")) {
                                System.out.println("Disponemos de tres formas de pago:");
                                System.out.println("1.Pago en cuotas");
                                System.out.println("2.Pago mensual");
                                System.out.println("3.Pago del curso completo");
                                System.out.println("Indique la opcion de pago que efectuara:");
                                int pe = sd.nextInt();
                                AdministradorPagos adp = new AdministradorPagos();
                                Pagos pg1 = adp.obtenerUtilidad(pe);
                                System.out.println("Usted efectuara el monto de:" + pg1.obtenerMontoTotal(cur1.getPrecio()));
                                sd.nextLine();
                                System.out.println("Desea efectuar este metodo de pago?:");
                                resp = sd.nextLine();

                            }
                            System.out.println("Registrado con exito");
                        }
                    }
                } else {
                    System.out.println("Opcion equivocada");
                }
            } else {
                System.out.println("Gracias por su visita");
            }
        }
    }

    public static void agregarCurso() {
        gc.agregarCurso();
    }

    public static void modificarCurso() {
        gc.modificarCurso();
    }

    public static void eliminarCurso() {
        gc.eliminarCurso();
    }
}
