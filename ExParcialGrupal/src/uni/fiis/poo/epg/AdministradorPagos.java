package uni.fiis.poo.epg;

import uni.fiis.poo.epg.im.PagoCuotas;
import uni.fiis.poo.epg.im.PagoCursoCompleto;
import uni.fiis.poo.epg.im.PagoMensual;

public class AdministradorPagos {

    public Pagos obtenerUtilidad(int op){
        Pagos respuesta = null;
        if(op==1){
            respuesta = new PagoCuotas();

        }else if(op==2){
            respuesta = new PagoMensual();

        }else if(op==3){
            respuesta=new PagoCursoCompleto();

        }else{
            System.out.println("No existe ese metodo de pago");
        }
        return respuesta;
    }
}
