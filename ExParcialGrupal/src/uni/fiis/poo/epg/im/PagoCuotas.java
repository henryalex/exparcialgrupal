package uni.fiis.poo.epg.im;


import uni.fiis.poo.epg.Pagos;

import java.util.Scanner;

public class PagoCuotas implements Pagos {

    public double obtenerMontoTotal(int costo) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Solo puede dividir en un maximo de ocho cuotas:");
        System.out.println("En cuantas cuotas desea dividir el monto total del curso?:");
        int n = sc.nextInt();
        double monto;
        double pago = 0;
        if (n > 4 && n <= 8) {
            monto = costo + 10 * n;
            pago = monto / n;
        } else if (n <= 4) {
            monto = costo + 5 * n;
            pago = monto / n;
        }
        return pago;

    }

    public void descuento() {

    }
}
