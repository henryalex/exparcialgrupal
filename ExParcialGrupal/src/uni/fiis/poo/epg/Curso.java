package uni.fiis.poo.epg;

import java.util.ArrayList;

public class Curso {
    private String codigo;
    private String nombre;
    private String descripcion;
    private String area;
    private int ciclo;
    private int precio;
    private int horasemanales;
    private ArrayList<String> temas;
    private ArrayList<Profesor> profesores;

    public Curso(String codigo, String nombre, String descripcion, String area, int ciclo, int precio, int horasemanales, ArrayList<String> temas, ArrayList<Profesor> profesores) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.area = area;
        this.ciclo = ciclo;
        this.precio = precio;
        this.horasemanales = horasemanales;
        this.temas = temas;
        this.profesores = profesores;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public void setHorasemanales(int horasemanales) {
        this.horasemanales = horasemanales;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getArea() {
        return area;
    }

    public int getPrecio() {
        return this.precio;
    }

    public ArrayList<Profesor> getProfesores() {
        return profesores;
    }

    @Override
    public String toString() {
        return "Curso{" +
                "nombre='" + nombre + '\'' +
                '}';
    }

    public void ObtenerhorariosPorProfe() {
        for (Profesor prof : this.profesores) {
            System.out.println(profesores.indexOf(prof)+1+".-"+prof.getNombre()+" "+prof.getApellido()+" "+prof.getHorarios());
        }
    }


}
